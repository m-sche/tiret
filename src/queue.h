/*
 * queue.h
 * Implements queue and stack as a doubly-linked list.
 *
 * 2014--2016 Martin Scheubrein
 */

#ifndef QUEUE_H
#define QUEUE_H

typedef struct queue {
	size_t size;
	struct queue_item *first;
	struct queue_item *last;
} queue_t;

void queue_init(queue_t *q);
size_t queue_size(queue_t *q);
void queue_push(queue_t *q, void *value);
void *queue_pop(queue_t *q);

void queue_unpop(queue_t *q, void *value);
void *queue_unpush(queue_t *q);

#endif
