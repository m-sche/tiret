#!/usr/bin/perl

use strict;
use warnings;

my $regex_stat = 0;	# 1 to print the regex statistics
$regex_stat = (@ARGV > 0 && $ARGV[0] eq 'regex');

open(my $log, '<', 'history.log') || die "Couldn't open history.log\n";

my @regexs = (
	["   date", "[0-9]{4}\]{0,2}( - )\[{0,2}[0-9]{1,2}\.", " – ", 0, 0],
	["  2 - 3", "[0-9]+\]{0,2}( - )\[{0,2}[0-9]+", "–", 0, 0],
	["2. - 3.", "[0-9]+\.\]{0,2}( - )\[{0,2}[0-9]+\.", "–", 0, 0],
	["  a - b", "[^0-9]+ (-),? [^0-9]+", "–", 0, 0],
	["    2-3", "[^-0-9][0-9]+\.?\]{0,2}(-)\[{0,2}[0-9]+[^-0-9]", "–", 0, 0],

	["PROTECT", "[0-9]{4}\]{0,2}( – )\[{0,2}[0-9]{1,2}\.", " – ", 0, 0],
	[" 2 -- 3", "[0-9]+\]{0,2}( – )\[{0,2}[0-9]+", "–", 0, 0],

	["     -5", " (-)[0-9]+", "−", 0, 0],
	["    +/-", "\\+/(-)", "−", 0, 0]
);

my $title_num = 0;
my $err_title_num = 0;
my $err_num = 0;
my $err_found_num = 0;
my $err_suggested_ok = 0;
my $ignored_title_num = 0;

my $title = "";
my $errnum = 1;	# Due to ig_tit_num
my $real_errnum = 0;

my $errfill = 0;
my $context_found;
my $context_suggested;

while (my $line = <$log>) {
	my ($type, $text) = parse_line($line);
	if ($type eq '@') {
		read_header($type, $text);
	} elsif ($type eq '$') {
		if ($text eq 'undone') {
			$err_found_num--;
			$errfill = 0;
		}
		# $err_suggested_ok--
	} else {
		$errfill++;
		if ($errfill == 1) {
			$context_found = $text;
		} elsif ($errfill == 2) {
			$context_suggested = $text;
		} elsif ($errfill == 3) {
			if ($context_suggested eq $text) {
				$err_suggested_ok++;
			}
			$errfill = 0;
			$err_num++;
			$real_errnum++;
			$regex_stat && regex_stat($context_found, $context_suggested, $text);
		}
	}
}

close($log);

print "\n";
print_result("errors found", $err_found_num);
print_result("- approved", $err_num, $err_found_num);
print_result("- easily", $err_suggested_ok, $err_num);
print "\n";
print_result("files examined", $title_num, 350000);	# Total articles
print_result("- errorneous", $err_title_num, $title_num);
print_result("- ignored", $ignored_title_num, $err_title_num);
print "\n";
print_result("errors per file", $err_found_num/$title_num);
print_result("- errorneous", $err_found_num/$err_title_num);
print_result("- accepted", $err_num/$err_title_num);
print "\n";

$regex_stat && print_regex_stat();

sub round
{
	return sprintf("%5.1f", $_[0]);
}

sub print_result
{
	my ($str, $n, $p) = @_;
	if  ($n == int($n)) {
		printf("%20s: %-5d ", $str, $n);
	} else {
		printf("%20s: %-5.1f ", $str, $n);
	}
	print $p ? round(100*$n/$p)." %\n" : "\n";
}

sub read_header
{
	my ($type, $text) = @_;
	my $new_title = $text;
	my $line = <$log>;
	$errnum = (parse_line($line))[1];

	if ($errnum == 0) {
		$title_num++;
		#print "0  $new_title\n"
	} elsif ($title eq $new_title) {
		#print "-- $new_title\n";
	} else {
		$err_found_num += $errnum;
		$title = $new_title;
		$title_num++;
		$err_title_num++;
		$ignored_title_num++ if ($real_errnum == 0);
		$real_errnum = 0;
		#printf("%2d %s\n", $errnum, $title);
	}
	$errfill = 0;
}

sub parse_line
{
	my ($line) = @_;
	chomp($line);
	my $type = substr($line, 0, 1);
	substr($line, 0, 2, "");
	return ($type, $line);
}

sub regex_stat
{
	my ($found, $suggested, $approved) = @_;
	foreach my $r (@regexs) {
		my ($key, $re, $sub) = @{$r};
		if ($found =~ /$re/) {
			#print "$key: $found\n";
			$r->[3]++;
			$r->[4]++ if ($suggested eq $approved);
			last;
		}
	}
}

sub print_regex_stat
{
	print "          regex   hits  correct\n";
	foreach my $r (@regexs) {
		my ($key, $re, $sub) = @{$r};
		printf("%15s: %5d  %-4.1f %%\n", $key, $r->[3], $r->[3] ? round(100*$r->[4]/$r->[3]) : 0);
	}
}
