/*
 * edit.c
 * All you need to edit an article on Wikipedia through the MediaWiki API.
 *
 * 2014--2016 Martin Scheubrein
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"

char *get_edit_token(CURL *curl, const char *base_url)
{
	char *url = strdup(base_url);
	char *str, *token;

	str_append(&url, "?action=query&meta=tokens&format=json");
	str = curl_request(curl, url, NULL, CURL_INSTANT);
	free(url);
	token = find_value(str, "query", "tokens", "csrftoken", NULL);
	free(str);
	token[strlen(token)-3] = '\0';
	str_append(&token, "%2B%5C");	/* Urlencode */

	return token;
}

int edit(CURL *curl, const char *base_url, struct page page,
		const char *section, const char *summary, const char *token)
{
	char *url;
	char *post;
	char *str;
	char *result;
	int retval = 0;

	url = strdup(base_url);
	str_append(&url, "?action=edit&format=json&minor&title=");

	/* Urlencoded title */
	str = curl_easy_escape(curl, page.title, 0);
	str_append(&url, str);
	curl_free(str);

	post = strdup("summary=");
	str_append(&post, summary);
	if (section) {
		str_append(&post, "&section=");
		str_append(&post, section);
	}

	/* Urlencoded content */
	str_append(&post, "&text=");
	str = curl_easy_escape(curl, page.content, 0);
	str_append(&post, str);
	curl_free(str);

	str_append(&post, "&basetimestamp=");
	str_append(&post, page.timestamp);
	str_append(&post, "&token=");	/* Should be the last parameter */
	str_append(&post, token);

	/* Send */
	str = curl_request(curl, url, post, 0);

	free(url);
	free(post);

	result = find_value(str, "error", NULL);
	if (result) {
		result = find_value(str, "error", "code", NULL);
		if (strcmp(result, "editconflict") == 0) {
			retval = ERR_EDIT_CONFLICT;
		} else {
			fprintf(stderr, "edit: error: %s\n", result);
			retval = ERR_EDIT_UNKNOWN;
		}
	} else {
		result = find_value(str, "warnings", NULL);
		if (result) {
			fprintf(stderr, "edit: warning: %s\n", result);
		} else {
			result = find_value(str, "edit", "result", NULL);
			fprintf(run_log, "edit: %s\n", result);
		}
	}
	free(result);
	free(str);
	return retval;
}
