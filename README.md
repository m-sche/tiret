# Tiret – A punctuation-correcting tool for Wikipedia

You came across a tool automatically searching for incorrectly used punctuation
marks, namely dash and hyphen, on the Czech edition of Wikipedia, the internet
encyclopædia. It facilitates and speeds up their retrieval and the commitment of
the corrective editations with the assistance of a~human corrector. It tries to
fix the errors itself and only asks for an approval.


## Compilation

To build the tool you need `glibc` (for regular expressions), `libcurl` and
`readline` libraries. The awesome [Serge Zaitsev](zserge.com/jsmn.html)'s `jsmn`
library is used too, but it is packed in this repository and there's no need for
a separate installation.

Before compilation, open the file `src/common.h` and locate the macro *LOGIN*.
Its default content is *m-sche*, which is my Wikipedia username. Chenge it to
your username.

Then you can compile the tool by a command `make`, which should create an
executable named `bot`. The tool must be run in a terminal since it uses a
command-line interface


## Use

If you want to see what the tool is doing in background, find the file `run.log`
in the root directory of this program (or create one if it doesn't exist). Into
this file, all important actions will be logged. You may want to run
`tail -f run.log` in a second window to see what actions are performed.

Run the program and type a password of your Wikipedia account. After logging in,
the program starts to scan Wikipedia, of which you are informed via `run.log`
file. Let the program run a few minutes, it may take several minutes to find an
errorneous article, but usually it's pretty fast, as the Wikipedia is full of
errors.

Then you can edit errors as long as you wish and quit the program by pressing
*Ctrl-x*. Unfortunately, there must be at least one error found for this
keyboard shortcut to work, so if there isn't any, please wait for a while and do
not kill it. The *Ctrl-x* quit performs some important tasks – it saves the
articles, which have been already downloaded or haven't been sent back yet. If
you killed the program by force, you would lose all this progress.


## User interface

The program shows one error per line. It uses a TeX-like notation of some signs.
Double hyphen *-\-* means an en-dash and a hyphen between two dollars *$-$* means
a minus. This is how the program shows you different punctuation and it also
accepts these characters only in this notation.

You will see the corrected version of the text and you only have to decide
whether the correction is acceptable or not. If yes, just press *Enter* and it's
approved. If not, you have several options to continue. First, you can edit the
line to a correct form (there's a cursor near the problematic part of the text)
and press *Enter* afterwise. Or, if you are not sure, you can skip this error by
pressing *Ctrl-i*. It tells the program not to change this part of the text. Or
you can skip the whole article by pressing *Ctrl-p* and make no changes in it at
all, whereby the errors you have already approved will be ignored too.

When you make a mistake and the article isn't sent yet, you can undo by *Ctrl-u*
and return to previous error. You go forward by pressing *Enter* again.

You quit the program by *Ctrl-x*. It usually takes a few seconds, because it
waits for all internet communication to finnish and it saves your progress.


## License

This whole thing is licensed under the 2-clause BSD license, see `LICENSE`.
It means you can do whatever you want with this code, but you have to mention my
contribution and attach that license to your project, if you redistribute it. And
please let me know of your project, I'd like to see what my work is useful for!
