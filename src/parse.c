/*
 * parse.c
 * Wrapper for jsmn parser for searching in a JSON structure.
 *
 * 2014--2016 Martin Scheubrein
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "jsmn.h"

static int jsmncmp(const char *json, jsmntok_t *tok, const char *str)
{
	return strncmp(json+tok->start, str, tok->end-tok->start);
}

static int parse_json(char *str, jsmntok_t **token)
{
	int toknum;
	jsmn_parser parser;

	jsmn_init(&parser);
	toknum = jsmn_parse(&parser, str, strlen(str), NULL, 0);
	if (toknum < 0) {
		fprintf(stderr, "(!) jsmn: failed to parse: %d\n", toknum);
		fprintf(stderr, ">>> %s <<<\n", str);
		return 0;
	}

	*token = malloc(toknum*sizeof(jsmntok_t));

	jsmn_init(&parser);
	jsmn_parse(&parser, str, strlen(str), *token, toknum);

	return toknum;
}

static int skip_children(jsmntok_t *token, size_t i)
{
	size_t size = token[i].size;
	size_t n;

	for (i++, n = 1; n <= size; n++)
		i = skip_children(token, i);
	return i;
}

# if 0
static void __attribute__((unused)) printtok(const char *str, jsmntok_t *token, size_t toknum)
{
	int i;

	puts("===========");
	for (i = 0; i < toknum; i++) {
		printf("token %2d> %.*s [%d]\n", i, token[i].end-token[i].start,
			str+token[i].start, token[i].size);
	}
	puts("===========\n");
}
#endif

/* If the needle is a void string, return first available value */
static int find_token(const char *str, jsmntok_t *token, size_t toknum, size_t i, const char *needle)
{
	size_t n;

	i++;

	if (token[i-1].type == JSMN_ARRAY) {
		/*puts("\nARRAY MODE\n");
		printf("needle = '%s'\n", needle);*/

		n = (size_t)strtol(needle, NULL, 10);
		if (n > token[i-1].size)
			return -1;

		while (n---1)
			i = skip_children(token, i);

		/*printf("val = %.*s [%d]\n", token[i].end-token[i].start,
			str+token[i].start, token[i].size);*/
		return i;
	}

	n = token[i-1].size;
	while (n-- && i < toknum) {
		/*printf("\nneedle = '%s'\n", needle);
		printf("key = '%.*s'\n", token[i].end-token[i].start, str+token[i].start);
		printf("val = %.*s [%d]\n", token[i+1].end-token[i+1].start,
			str+token[i+1].start, token[i+1].size);*/

		if (token[i].type == JSMN_STRING
		&& (needle[0] == '\0'
		|| jsmncmp(str, &token[i], needle) == 0))
			return i+1;

		if (token[i+1].type == JSMN_OBJECT
		|| token[i+1].type == JSMN_ARRAY)
			i = skip_children(token, i+1);
		else
			i += 2;
	}

	return -1;
}

char *find_value(char *str, ...)
{
	char *needle;
	char *value;
	size_t found = 0;
	size_t toknum;
	jsmntok_t *token = NULL;
	va_list args;

	va_start(args, str);
	toknum = parse_json(str, &token);
	if (toknum == 0) {
		free(token);
		return NULL;
	}
	//printtok(str, token, toknum);

	while (found != -1) {
		needle = va_arg(args, char *);
		if (needle == NULL)
			break;
		found = find_token(str, token, toknum, found, needle);
	}

	va_end(args);
	if (found == -1) {
		free(token);
		//fprintf(stderr, "(!) jsmn: key '%s' not found\n", needle);
		return NULL;
	}

	value = strndup(str+token[found].start, token[found].end-token[found].start);
	free(token);
	return value;
}
