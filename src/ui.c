/*
 * ui.c
 * User interface, approving the errors.
 *
 * 2014--2016 Martin Scheubrein
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <readline/readline.h>
#include <sys/ioctl.h>	/* terminal_width */
#include <unistd.h>	/* terminal_width */
#include <signal.h>	/* terminal_width */
#include "common.h"
#include "heap.h"
#include "queue.h"

static int quit_flag;
static int winch_flag;

static void winch(int sig)
{
	winch_flag = 1;
}

static int term_width(void)
{
	struct winsize w;
	static int cols = 0;

	if (!cols) {
		signal(SIGWINCH, winch);
		winch_flag = 1;
	}

	if (winch_flag) {
		winch_flag = 0;
		ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
		cols = w.ws_col;
	}

	return cols;
}

static int context_size(void)
{
	int real = term_width();
	int bounded = real < 80 ? 80 : real > 200 ? 200 : real;
	return bounded-5;
}

static void texize(char **str)
{
	char *tmp;
	tmp = replace(*str, "–", "--");
	free(*str);
	*str = replace(tmp, "−", "$-$");
	free(tmp);
}

static void detexize(char **str)
{
	char *tmp;
	tmp = replace(*str, "--", "–");
	free(*str);
	*str = replace(tmp, "$-$", "−");
	free(tmp);
}

/* Returns the corrected line */
static char *strdup_context(char *str, size_t from, size_t to, struct error *err)
{
	char *buffer = NULL;

	str_n_append(&buffer, str+from, err->index-from);
	str_append(&buffer, err->replacement);
	str_n_append(&buffer, str+err->index+err->size, to-err->index-err->size);

	return buffer;
}

/* Compare the string 'human' to a str[from]/str[to] slice and generate a struct error */
static struct error *error_from_context(char *human, char *str, size_t from, size_t to)
{
	size_t index = 0;
	size_t end = 1;
	size_t human_end = strlen(human);
	size_t size;

	if (strncmp(human, str+from, human_end) == 0)
		return NULL;

	/* Find the start of difference */
	while (index < to-from && str[from+index] == human[index])
		index++;

	/* Find the end of difference */
	while (human_end-end > index && str[to-end] == human[human_end-end])
		end++;

	size = human_end-end-index+1;

	//printf("error '%.*s' to '%.*s'\n", to-from-end-index+1, str+from+index, size, human+index);
	struct error *err = malloc(sizeof(struct error));
	err->index = from+index;
	err->size = to-from-end-index+1;
	err->replacement = strndup(human+index, size);
	return err;
}

/* Readline garbage */

static struct rl_args {
	char *str;
	int cursor;
} rl_args;

static int ignore_flag;	/* Ignore this article */
static int skip_flag;	/* Skip this error */
static int undo_flag;	/* Back to the previous error */

static int readline_keyseq_handle(int count, int key)
{
	rl_replace_line("", 0);
	rl_redisplay();
	switch (key) {
	case 9:
		skip_flag = 1;
		break;
	case 16:
		ignore_flag = 1;
		break;
	case 21:
		undo_flag = 1;
		break;
	case 256:
		quit_flag = 1;
		break;
	}
	rl_done = 1;
	return 0;
}

static int readline_start(void)
{
	rl_insert_text(rl_args.str);
	rl_point = rl_args.cursor;
	rl_bind_keyseq("\\C-x", readline_keyseq_handle);
	rl_bind_keyseq("\\C-i", readline_keyseq_handle);
	rl_bind_keyseq("\\C-p", readline_keyseq_handle);
	rl_bind_keyseq("\\C-u", readline_keyseq_handle);
	return 0;
}

static struct error *human(char *str, struct error *err)
{
	size_t from, to;
	struct error *out;

	context(str, err, &from, &to, context_size());

	/* Start the interface */

	rl_args.str = strdup_context(str, from, to, err);
	texize(&rl_args.str);
	/* TODO Cursor position may be shifted by texize() */
	rl_args.cursor = err->index-from+strlen(err->replacement);
	rl_startup_hook = readline_start;

	char *line = readline(NULL);

	/* Quit if user aborts the program */
	if (quit_flag) {
		puts("[*  ] Exitting the user interface");
		return NULL;
	}

	/* Skip this error */
	if (undo_flag || ignore_flag || skip_flag) {
		free(rl_args.str);
		free(line);
		return NULL;
	}

	detexize(&line);
	free(rl_args.str);

	/* Generate the approved error */
	out = error_from_context(line, str, from, to);

	/* Log the error */
	if (out) {
		rl_args.str = strdup_context(str, from, to, err);

		flockfile(history_log);
		log_history("# %.*s%.*s%.*s\n",
			err->index-from, str+from,
			err->size, str+err->index,
			to-err->index-err->size, str+err->index+err->size);
		log_history("# %s\n", rl_args.str);
		log_history("# %s\n", line);
		funlockfile(history_log);

		free(rl_args.str);
	}

	free(line);
	return out;
}

int ui_approve(struct page *page)
{
	log_history("@ %s\n@ %u\n", page->title, heap_size(&page->err_found));

	while (heap_size(&page->err_found)) {
		struct error *err = heap_pop(&page->err_found);
		struct error *out = human(page->content, err);

		if (out) {
			/* Normal approval */
			queue_push(&page->err_approved, (void *)out);
		} else if (quit_flag) {
			/* Quit the program */
			heap_push(&page->err_found, (void *)err);
			return 1;
		} else if (ignore_flag) {
			/* Ignore this page, free all */
			free(err);
			while (heap_size(&page->err_found))
				free(heap_pop(&page->err_found));
			while (queue_size(&page->err_approved)) {
				err = queue_pop(&page->err_approved);
				free(err->replacement);
				free(err);
			}
			ignore_flag = 0;
			log_history("$ ignored page %s\n", page->title);
			return 0;
		} else if (undo_flag) {
			/* Undo an error */ /* TODO Leak in err->replacement */
			heap_push(&page->err_found, (void *)err);
			if (queue_size(&page->err_approved)) {
				heap_push(&page->err_found, queue_unpush(&page->err_approved));
				log_history("$ undone\n");
			}
			undo_flag = 0;
			continue;
		} else if (skip_flag) {
			/* Skip this error */
			err->replacement = strndup(page->content+err->index, err->size);
			queue_push(&page->err_approved, (void *)err);
			skip_flag = 0;
			continue;
		}
		free(err);
	}

	/* Delete skipped errors */
	size_t i;
	for (i = queue_size(&page->err_approved); i; i--) {
		struct error *err = queue_pop(&page->err_approved);
		if (strncmp(page->content+err->index, err->replacement, err->size)) {
			queue_push(&page->err_approved, (void *)err);
		} else {
			free(err->replacement);
			free(err);
		}
	}
	return 0;
}
