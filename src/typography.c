/*
 * typography.c
 * Forbidding the dangerous area, finding the errors in text. This is where all
 * regular expressions are placed -- look at find_forbidden() and find_errors().
 *
 * 2014--2016 Martin Scheubrein
 */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <regex.h>
#include "common.h"
#include "heap.h"
#include "queue.h"

/* Tight context where puctuation is considered valid for a break */
static const int context_accept_punct = 10;

static void right_context(char *str, struct error *err, size_t *to, int context)
{
	const size_t left = err->index+err->size;
	size_t i;

	*to = left;
	for (i = left; i < strlen(str) &&
	mb_strnlen(str+left, i-left) < context; i++) {	/* i-left < context */
		if (ispunct(str[i]) && *to-left < context_accept_punct)
			*to = i;
		if (isspace(str[i])) {
			*to = i;
			if (str[i] == '\n')
				break;
		}
	}
}

static void left_context(char *str, struct error *err, size_t *from, int context)
{
	size_t i;

	*from = err->index;
	for (i = err->index; mb_strnlen(str+i, err->index-i) <= context; i--) {
		//printf("left: i = %u, mblen = %u\n", i, mb_strnlen(str+i, err->index-i));
		if (ispunct(str[i]) && err->index-*from+1 < context_accept_punct)
			*from = i+1;
		if (isspace(str[i])) {
			*from = i+1;
			if (str[i] == '\n')
				break;
		}

		/* Because size_t is unsigned */
		if (i == 0) {
			*from = 0;
			break;
		}
	}
}

/* Return the optimal context boundaries to the variables 'from' and 'to' */
void context(char *str, struct error *err, size_t *from, size_t *to, size_t context)
{
	/* Right side has a right to have a maximum of half of the context */
	right_context(str, err, to, context/2);

	/* Left side slurps the rest -- it is more important */
	left_context(str, err, from, context-mb_strnlen(str+*to, *to-err->index));
	// context+err->index-*to

	/* If the left side breaks early, the right side may take the rest */
	right_context(str, err, to, context-mb_strnlen(str+*from, err->index+err->size-*from));
	// context+ *from-err->index-err->size

	//printf("%d -- %d\n", err->index-*from, *to-err->index-err->size);
}

/*
 * Like strstr(), but ignore everything flagged 'ignore' in 'forbidden' array;
 * 'str' points to the whole string, 'haystack' points to the search beginning
 */
static char *fb_strstr(char *str, char *haystack, char *needle, int *forbidden, int ignore)
{
	char *res = strstr(haystack, needle);
	while (res && (forbidden[res-str] & ignore)) {
		//printf("%s on %d forbidden by 0x%x: %.20s...\n", needle, res-str, forbidden[res-str], res);
		res = strstr(res+1, needle);
	}
	return res;
}

/*
 * In a string 'str', from 'start', find a matching 'right_str' to 'left_str'
 * and ignore everything, which is flagged 'ignore' in 'fb' flag array
 */
static char *matching_pair(char *str, char *start, int *fb, char *left_str, char *right_str, int ignore)
{
	int lvl = 0;
	size_t r_len = strlen(right_str);
	size_t l_len = strlen(left_str);
	char *left = fb_strstr(str, start, left_str, fb, ignore);
	char *right = fb_strstr(str, start, right_str, fb, ignore);

	while (right) {
		if (left && left < right) {
			lvl++;
			left = fb_strstr(str, left+l_len, left_str, fb, ignore);
		} else {
			if (--lvl <= 0)
				break;
			right = fb_strstr(str, right+r_len, right_str, fb, ignore);
		}
	}
	return right ? right+r_len : NULL;
}

#define FB_PRE		0x0001
#define FB_NOWIKI	0x0002
#define FB_LINK		0x0004
#define FB_TEMPLATE	0x0008
#define FB_MATH		0x0010
#define FB_REGEX	0x0020
#define FB_GALLERY	0x0040
#define FB_HTML_TAG	0x0080
#define FB_REF		0x0100
#define FB_CODE		0x0200
#define FB_LITERATURE	0x0400
#define FB_TIMELINE	0x0800

#define FB_ALL		0x0FFF

static void memflag(int *str, int flag, size_t n)
{
	while (n--)
		*(str++) |= flag;
}

/*
 * Mark areas of 'left_str' up to 'right_str' with 'flag',
 * areas of 'ignore' have already been forbidden
 */
static void forbid(struct page *page, char *left_str, char *right_str, int flag, int ignore)
{
	char *left;
	char *right;

	left = fb_strstr(page->content, page->content, left_str, page->forbidden, ignore);
	//printf("left  %s %d\n", left_str, left ? left-page->content : -1);
	while (left) {
		right = matching_pair(page->content, left,
			page->forbidden,
			left_str, right_str, ignore);
		//printf("right %s %d\n", right_str, right ? right-page->content : -1);
		if (!right)
			break;

		//printf("pair %.*s\n", right-left, left);
		memflag(page->forbidden+(left-page->content), flag, right-left);
		left = fb_strstr(page->content, right, left_str,
			page->forbidden, ignore);
		//printf("left  %s %d\n", left_str, left ? left-page->content : -1);
	}
}


#if 0 /* TODO Rewrite forbid() to accept regular expressions */
static regmatch_t fb_regex(char *haystack, regex_t *needle, int *forbidden, int ignore)
{
	char *str = haystack;
	regmatch_t match[1];

	while (regexec(needle, str, 1, match, 0) == 0) {
		size_t offset = str+match[0].rm_so-haystack;
		size_t size = match[0].rm_eo-match[0].rm_so;

		if (!(forbidden[offset] & ignore))
			return match;
		str += match[0].rm_eo;
	}
	return NULL;
}

static char *matching_regex(char *str, int *fb, regex_t *left_str, regex_t *right_str, int ignore)
{
	int lvl = 0;
	char *left = fb_strstr(str, left_str, fb, ignore);
	char *right = fb_strstr(str, right_str, fb, ignore);

	while (right) {
		if (left && left < right) {
			lvl++;
			left = fb_regex(left+1, left_str, fb, ignore);
		} else {
			if (--lvl == 0)
				break;
			right = fb_strstr(right+1, right_str, fb, ignore);
		}
	}
	return right ? right+r_len : NULL;
}

static void forbid_regex(struct page *page, char *left_str, char *right_str, int flag, int ignore)
{
	char *left;
	char *right;
	regex_t left_regex;
	regex_t right_regex;

	regcomp(&left_regex, left_str, REG_EXTENDED);
	regcomp(&right_regex, right_str, REG_EXTENDED);

	left = fb_regex(page->content, left_regex, page->forbidden, ignore);
	while (left) {
		right = matching_pair(left, page->forbidden+(left-page->content),
						left_regex, right_regex, ignore);
		if (!right)
			break;

		printf("pair %.*s\n", right-left, left);
		memflag(page->forbidden+(left-page->content), flag, right-left);
		left = fb_strstr(right, left_regex, page->forbidden+(right-page->content), ignore);
	}

	regfree(&left_regex);
	regfree(&right_regex);
}
#endif

/* For debugging purposes only */
static void __attribute__((unused)) print_forbidden(struct page *page)
{
	const size_t length = strlen(page->content);
	size_t i = 0;
	int fb_on = 0;
	int color = 1;

	while (i < length) {
		if (fb_on && !page->forbidden[i]) {
			printf("\033[0m");
			fb_on = 0;
		} else {
			if ((!fb_on && page->forbidden[i])
			|| (fb_on && fb_on != page->forbidden[i])) {
				printf("\033[3%d;2m", color);
				fb_on = page->forbidden[i];
				color = color == 1 ? 3 : 1;
			}
		}
		putchar(page->content[i++]);
	}
	printf("\033[0m\n");
}

/* Populate the page->forbidden element */
void find_forbidden(struct page *page)
{
	/* Possibly dangerous tables {| |} */
	forbid(page, "<pre>", "</pre>", FB_PRE, 0);
	forbid(page, "<nowiki>", "</nowiki>", FB_NOWIKI, FB_PRE);
	forbid(page, "<math>", "</math>", FB_MATH, FB_PRE);
	forbid(page, "<gallery", "</gallery>", FB_GALLERY, FB_PRE);
	forbid(page, "<timeline>", "</timeline>", FB_TIMELINE, FB_PRE);
	forbid(page, "<ref>", "</ref>", FB_REF, FB_PRE);
	forbid(page, "<code>", "</code>", FB_CODE, FB_PRE);
	forbid(page, "<source", "</source>", FB_CODE, FB_PRE);

	// TODO == Literature == \n\n content \n\n
	forbid(page, "== Literatura ==", "\n\n", FB_LITERATURE, FB_PRE | FB_NOWIKI);
	forbid(page, "==Literatura==", "\n\n", FB_LITERATURE, FB_PRE | FB_NOWIKI);
	forbid(page, "==Reference==", "\n\n", FB_REF, FB_PRE | FB_NOWIKI);
	forbid(page, "== Reference ==", "\n\n", FB_REF, FB_PRE | FB_NOWIKI);

	//forbid_regex(page, "[", "]", FB_LINK, FB_ALL);

	forbid(page, "{{", "}}", FB_TEMPLATE, FB_ALL);
	forbid(page, "[", "]", FB_LINK, FB_ALL);
	forbid(page, "<", ">", FB_GALLERY, FB_ALL);

	//print_forbidden(page);
}

static void find_regex(struct page *page, const char *regex, char *replacement)
{
	char *str = page->content;
	const int width = 1;	/* For debugging pusposes use 0, otherwise 1 */

	regex_t compiled;
	regmatch_t match[2];
	regcomp(&compiled, regex, REG_EXTENDED);

	while (regexec(&compiled, str, 2, match, 0) == 0) {
		size_t offset = str+match[width].rm_so-page->content;
		size_t size = match[width].rm_eo-match[width].rm_so;

		/* If the found area lies off the forbidden zone, continue */
		if (!(page->forbidden[offset] & FB_ALL)) {
			memflag(page->forbidden+offset, FB_REGEX, size);
			/* If the regex corrects something, create an error */
			if (strncmp(page->content+offset, replacement, size)) {
				//printf("error from %u, size %u\n", offset, size);
				struct error *err = malloc(sizeof(struct error));
				err->index = offset;
				err->size = size;
				err->replacement = replacement;
				heap_push(&page->err_found, (void *)err);
			}
		}
		str += match[1].rm_eo;
	}

	regfree(&compiled);
}

/* Hyphen - dash – minus − */

/* TODO List
 * &nbsp;
 * Compile only once
 * 2004-today
 * [[do not correct|correct this]]
 * <ref name="">
 *
 * Other non-tiret features
 * &nbsp;%
 * x --> times
 */

void find_errors(struct page *page)
{
	/* D. M Y - D. M Y => D. M Y -- D. M Y */
	/* The spaces are because of forbidding the next regex */
	find_regex(page, "[[:digit:]]{4}\\]{2}?( - )\\[{2}?[[:digit:]]{1,2}\\.", " – ");

	/* 2 - 3 => 2--3 */
	//find_regex(page, "[[:digit:]]+( - )[[:digit:]]+", "–");
	find_regex(page, "[[:digit:]]+\\]{2}?( - )\\[{2}?[[:digit:]]+", "–");

	/* 2. - 3. => 2.--3. */
	find_regex(page, "[[:digit:]]+\\.\\]{2}?( - )\\[{2}?[[:digit:]]+\\.", "–");

	/* A - B => A--B not very useful */
	//find_regex(page, "[[:upper:]][[:alpha:]]+( - )[[:upper:]][[:alpha:]]+", "–");

	/* a - b => a -- b */
	/* The second space is non-breakable */
	find_regex(page, "[[:graph:]]+[  ](-),? [[:graph:]]+", "–");

	/* 2-3 => 2--3 */
	find_regex(page, "[^-[:digit:]][[:digit:]]+\\.?\\]{2}?(-)\\[{2}?[[:digit:]]+[^-[:digit:]]", "–");

	/* 2 -- 3 => 2--3 */
	/* First, protect the headers */
	find_regex(page, "[[:digit:]]{4}\\]{2}?( – )\\[{2}?[[:digit:]]{1,2}\\.", " – ");
	find_regex(page, "[[:digit:]]+\\]{2}?( – )\\[{2}?[[:digit:]]+", "–");

	/* -5 => $-5$ */
	find_regex(page, "[ [:punct:]](-)[[:digit:]]+", "−");

	/* +/− in sports tables */
	find_regex(page, "\\+/(-)", "−");
}

/* Apply approved errors to the original text and create the repaired version */
/* TODO optimize */
char *repair(char *str, queue_t *errors)
{
	char *correct = NULL;
	size_t i = 0;
	struct error *err = NULL;
	unsigned int unprocessed = queue_size(errors);

	while (unprocessed) {
		if (err == NULL)
			err = queue_pop(errors);

		if (i == err->index) {

#if defined(FAKE_EDIT) && !defined(CHECK_IO)
			str_append(&correct, "\033[42;1m");
			str_append(&correct, err->replacement);
			str_append(&correct, "\033[0m");
#else
			str_append(&correct, err->replacement);
#endif
			i += err->size;

			queue_push(errors, err);	//free(err);
			err = NULL;
			unprocessed--;
		} else {
			str_n_append(&correct, str+i, err->index-i);
			i = err->index;
		}
	}
	str_append(&correct, str+i);

	return correct;
}
