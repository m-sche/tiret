/*
 * main.c
 * Implements the main three threads -- I/O/User, starting and shutting down.
 *
 * 2014--2016 Martin Scheubrein
 */

#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <pthread.h>
#include <locale.h>
#include <unistd.h>
#include <stdarg.h>
#include "common.h"
#include "queue.h"

CURL *curl;

const char *wiki_api = "https://cs.wikipedia.org/w/api.php";
char *edit_token;

queue_t output_queue;
queue_t input_queue;

pthread_mutex_t output_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t input_mutex = PTHREAD_MUTEX_INITIALIZER;

pthread_cond_t output_queue_growth = PTHREAD_COND_INITIALIZER;
pthread_cond_t input_queue_shrinkage = PTHREAD_COND_INITIALIZER;
pthread_cond_t input_queue_growth = PTHREAD_COND_INITIALIZER;

FILE *run_log;		/* Non-persistent, queue changes + network I/O */
FILE *history_log;	/* Persistent, titles + found errors */


void log_history(const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
#ifndef FAKE_LOG
	vfprintf(history_log, fmt, args);
#else
	vfprintf(run_log, fmt, args);
#endif
	va_end(args);
}

/* Fancy stars for better user orientation */
static void log_q_size(queue_t *q, const char *name)
{
	size_t i;

	flockfile(run_log);

	fprintf(run_log, "%s: [", name);
	for (i = 0; i < INPUT_THRESHOLD; i++)
		fputc(i < queue_size(q) ? '*' : ' ', run_log);
	fputs("]\n", run_log);

	funlockfile(run_log);
}

/* Free the whole page structure */
static void destroy_page(struct page *page)
{
	while (heap_size(&page->err_found))
		free(heap_pop(&page->err_found));
	heap_destroy(&page->err_found);
	while (queue_size(&page->err_approved)) {
		struct error *err = queue_pop(&page->err_approved);
		free(err->replacement);
		free(err);
	}
	free(page->title);
	free(page->content);
	free(page->forbidden);
	free(page->timestamp);
}

#ifdef MONOLITHIC
static void process_page(CURL *curl, struct page *page, const char *api, const char *edit_token)
{
	//printf("%s\n", page->content);
	printf("=== %s ===\n", page->title);
	printf(">> timestamp: %s\n", page->timestamp);
	printf(">> errors found: %d\n\n", heap_size(&page->err_found));

	ui_approve(page);

	printf(">> errors approved: %d\n\n", queue_size(&page->err_approved));

	char *rep = repair(page->content, &page->err_approved);
	free(page->content);
	page->content = rep;

#ifdef FAKE_EDIT
	puts(page->content);
	putchar('\n');
#else
	char *summary = "Typografie";
	edit(curl, api, *page, NULL, summary, edit_token); // NULL, "new", "0"
#endif

	destroy_page(page);
}
#endif /* MONOLITHIC */

static void *output(void *arg)
{
	struct page *page;
	int edit_error = 0;

	pthread_mutex_lock(&output_mutex);
	for (;;) {
		while (queue_size(&output_queue)) {
			pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
			page = (struct page *)queue_pop(&output_queue);
			fprintf(run_log, "output: processing '%s'\n", page->title);
			pthread_mutex_unlock(&output_mutex);

			/* Change the text accordingly to the errors */
			char *rep = repair(page->content, &page->err_approved);
			free(page->content);
			page->content = rep;

#ifdef CHECK_IO
			char *fn = NULL;
			str_append(&fn, "tmp/");
			str_append(&fn, page->title);
			str_append(&fn, "-edit");
			FILE *f = fopen(fn, "a");
			free(fn);
			fputs(page->content, f);
			fclose(f);
#endif

#ifndef FAKE_EDIT
			/* 4th arg: NULL = all, "new" = new section, "0" = intro section */
			edit_error = edit(curl, wiki_api, *page, NULL, "Typografie", edit_token);
#endif

			/* User may have been cut off due to inactivity */
			if (edit_error) {
				fprintf(run_log, "(!) output: '%s' failed\n", page->title);

				switch (edit_error) {
				case ERR_EDIT_CONFLICT:
					fprintf(run_log, "output: ignoring edit conflict on '%s'\n", page->title);
					destroy_page(page);	/* Give up on this */
					break;
				case ERR_EDIT_UNKNOWN:
				default:
					/* Return it back into the output queue */
					pthread_mutex_lock(&output_mutex);
					queue_unpop(&output_queue, (void *)page);
					pthread_mutex_unlock(&output_mutex);
					break;
				}

			} else {
				fprintf(run_log, "output: '%s' sent\n", page->title);
				destroy_page(page);	/* Free all resources of page */
			}

			pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
			pthread_testcancel();
			pthread_mutex_lock(&output_mutex);
		}
		pthread_cond_wait(&output_queue_growth, &output_mutex);
	}
	return NULL;
}

static void *input(void *arg)
{
	struct page *page;

	pthread_mutex_lock(&input_mutex);
	for (;;) {
		while (queue_size(&input_queue) < INPUT_THRESHOLD) {
			pthread_mutex_unlock(&input_mutex);
			pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

			/* Download the page */
			page = malloc(sizeof(struct page));
#ifdef FAKE_QUERY
			*page = query(curl, wiki_api, FAKE_QUERY);
#else
  #ifdef FAKE_TITLE
			*page = query(curl, wiki_api, FAKE_TITLE);
  #else
			*page = get_page(curl, wiki_api);
  #endif /* FAKE_TITLE */
#endif /* FAKE_QUERY */
			/* Parsing error etc. */
			if (page->content == NULL) {
				free(page);
				fprintf(run_log, "(!) input: page retrieval failed\n");
				pthread_mutex_lock(&input_mutex);
				continue;
			}

			/* Find errors, repeat when none found */
			find_forbidden(page);
			find_errors(page);
			if (heap_size(&page->err_found) == 0) {
				fprintf(run_log, "input: nothing in '%s'\n", page->title);
				log_history("@ %s\n@ 0\n", page->title);
				destroy_page(page);
				pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
				pthread_testcancel();
				pthread_mutex_lock(&input_mutex);
				continue;
			}

			/* If any error is found, add the page into the input queue */

			pthread_mutex_lock(&input_mutex);

			fprintf(run_log, "input: forwarding '%s' with %zu errors\n",
				page->title, heap_size(&page->err_found));
			queue_push(&input_queue, (void *)page);
			log_q_size(&input_queue, "articles ready");

			pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
			pthread_cond_signal(&input_queue_growth);
		}
		pthread_cond_wait(&input_queue_shrinkage, &input_mutex);
	}
	return NULL;
}

static void ui(void)
{
	struct page *page;

	for (;;) {
		pthread_mutex_lock(&input_mutex);

		/* No errorneous pages */
		if (queue_size(&input_queue) == 0) {
			printf("I haven't found anything, wait a moment...\n");
			fflush(stdout);
			pthread_cond_wait(&input_queue_growth, &input_mutex);
			/* TODO Allow user to quit there */
		}

		page = (struct page *)queue_pop(&input_queue);	/* Pop a page */
		log_q_size(&input_queue, "articles ready");
		pthread_mutex_unlock(&input_mutex);
		pthread_cond_signal(&input_queue_shrinkage);

		/* Approval process */
		printf("\n=== %s ===\n", page->title);
		//printf(">> timestamp: %s\n", page->timestamp);
		printf(">> errors found: %zu\n\n", heap_size(&page->err_found));

		if (ui_approve(page)) {
			/* Push the unfinished page back when exitting */
			pthread_mutex_lock(&input_mutex);
			queue_unpop(&input_queue, (void *)page);
			pthread_mutex_unlock(&input_mutex);
			return;
		}

		/* No errors are valid */
		if (queue_size(&page->err_approved) == 0) {
			destroy_page(page);
			continue;
		}

		/* Push approved page to the output queue */
		pthread_mutex_lock(&output_mutex);
		queue_push(&output_queue, (void *)page);
		pthread_mutex_unlock(&output_mutex);
		pthread_cond_signal(&output_queue_growth);
	}
}


int main(int argc, char **argv)
{
	/* Must be an utf8 encoding due to UI */
	if (setlocale(LC_CTYPE, "cs_CZ.utf8") == NULL) {
		puts("(!) setlocale(): locale cs_CZ.utf8 not found");
		return 1;
	}

	run_log = fopen("run.log", "w");
	setbuf(run_log, NULL);
	history_log = fopen("history.log", "a");
	setbuf(history_log, NULL);

#ifndef FAKE_QUERY
	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();
	if (curl == NULL) {
		fprintf(stderr, "(!) curl: curl_easy_init() failed\n");
		return 1;
	}

	curl_easy_setopt(curl, CURLOPT_COOKIEFILE, "");
#endif /* FAKE_QUERY */

#ifndef FAKE_EDIT
	char *password = getpass("password for " LOGIN ": ");
	puts("[*   ] Logging into Wikipedia");
	login(curl, wiki_api, LOGIN, password);
	puts("[**  ] Getting the edit token");
	edit_token = get_edit_token(curl, wiki_api);
	fprintf(run_log, "edit token: %s\n", edit_token);
#endif /* FAKE_EDIT */


#ifdef MONOLITHIC

#ifdef FAKE_TITLE
	struct page page = query(curl, wiki_api, FAKE_TITLE);
#else
  #ifdef FAKE_QUERY
	struct page page = query(curl, wiki_api, FAKE_QUERY);
  #else
	struct page page = get_page(curl, wiki_api);
  #endif /* FAKE_QUERY */
#endif /* FAKE_TITLE */

	if (page.content == NULL)
		return 1;

	find_forbidden(&page);
	find_errors(&page);
	process_page(curl, &page, wiki_api, edit_token);

#else /* not MONOLITHIC */

	pthread_t output_thread;
	pthread_t input_thread;

	queue_init(&output_queue);
	queue_init(&input_queue);

#ifndef FAKE_SAVE
	puts("[*** ] Loading the saves");
	load_pages("saves/input_queue", &input_queue);
	load_pages("saves/output_queue", &output_queue);
	/* Better to lose work than to re-edit in case of an unexpected quit */
	unlink("saves/input_queue");
	unlink("saves/output_queue");
#endif

	puts("[****] Starting the I/O threads");
	pthread_create(&output_thread, NULL, output, NULL);
	pthread_create(&input_thread, NULL, input, NULL);

	ui();

	puts("[** ] Waiting for I/O threads to quit");
	pthread_cancel(output_thread);
	pthread_join(output_thread, NULL);
	pthread_cancel(input_thread);
	pthread_join(input_thread, NULL);

#ifndef FAKE_SAVE
	puts("[***] Saving the unprocessed pages");
	save_pages("saves/input_queue", &input_queue);
	save_pages("saves/output_queue", &output_queue);
#endif


#endif /* not MONOLITHIC */


#ifndef FAKE_QUERY
	curl_easy_cleanup(curl);
	curl_global_cleanup();
#endif /* FAKE_QUERY */

	fclose(history_log);
	fclose(run_log);

	return 0;
}
