/*
 * login.c
 * Log the user into Wikipedia.
 *
 * 2014--2016 Martin Scheubrein
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include "common.h"

void login(CURL *curl, const char *base_url, const char *username, const char *password)
{
	char *url = strdup(base_url);
	char *post = NULL;
	char *value;
	char *str;

	str_append(&url, "?action=login&format=json");

	str_append(&post, "lgname=");
	str_append(&post, username);
	str_append(&post, "&lgpassword=");
	str_append(&post, password);

	str = curl_request(curl, url, post, CURL_INSTANT);
	value = find_value(str, "login", "result", NULL);

	fprintf(run_log, "login: %s\n", value);

	if (strcmp(value, "NeedToken") == 0) {
		free(value);
		str_append(&post, "&lgtoken=");
		value = find_value(str, "login", "token", NULL);
		value[strlen(value)-3] = '\0';
		str_append(&value, "%2B%5C");	/* urlencode */
		fprintf(run_log, "login: token: %s\n", value);
		str_append(&post, value);

		/* Send the login token */
		free(str);
		str = curl_request(curl, url, post, CURL_INSTANT);

		free(value);
		value = find_value(str, "login", "result", NULL);

		fprintf(run_log, "login: %s\n", value);
	}

	free(url);
	free(post);

	if (strcmp(value, "Success") != 0) {
		fprintf(stderr, "(!) login failed: %s\n",
			find_value(str, "login", "result", NULL));
		exit(1);
	}

	free(str);
	free(value);
}
