# Makefile
# 2014--2016 Martin Scheubrein


CC	= gcc

CFLAGS	= -Wall -O2

LDFLAGS	= -lcurl -lpthread -lreadline

#-pg -ftest-coverage -fprofile-arcs
GPROF	=

# TODO split into api.o and therest.o

SRC	= $(patsubst src/%.c,%.c,$(wildcard src/*.c))
OBJ	= $(addprefix tmp/,$(SRC:.c=.o)) libjsmn.a

bot: $(OBJ)
	$(CC) $(LDFLAGS) $(GPROF) $^ -o $@

tmp/%.o: src/%.c src/common.h Makefile
	$(CC) $(CFLAGS) $(GPROF) -c $< -o $@

tmp/queue.o: src/queue.h
tmp/parse.o: src/jsmn.h
src/jsmn.h: jsmn/jsmn.h


libjsmn.a: jsmn/libjsmn.a
	cp $< .
jsmn/libjsmn.a: jsmn/jsmn.c jsmn/jsmn.h
	cd jsmn && $(MAKE)
jsmn.h: jsmn/jsmn.h
	cp $< .


.PHONY: test leak fulleak clean

test: bot
	./bot

leak: bot
	valgrind --leak-check=full --track-origins=yes ./bot

clean:
	rm -f tmp/* bot gmon.out run.log
#*.gcno *.gcda *.gcov
