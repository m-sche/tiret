/*
 * save.c
 * Save & load the articles which have been already downloaded on exit.
 *
 * 2014--2016 Martin Scheubrein
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "common.h"
#include "heap.h"
#include "queue.h"

/** Saving **/

static void save_string(FILE *file, char *str)
{
	size_t len = strlen(str);
	fwrite(&len, sizeof(size_t), 1, file);
	fwrite(str, 1, len, file);
}

static void save_error(FILE *file, struct error *err)
{
	fwrite(&err->index, sizeof(size_t), 1, file);
	fwrite(&err->size, sizeof(size_t), 1, file);
	save_string(file, err->replacement);
	//free(err->replacement); /* TODO May be static */
}

static void save_err_heap(FILE *file, heap_t *heap)
{
	size_t len;
	struct error *err;

	len = heap_size(heap);
	fwrite(&len, sizeof(size_t), 1, file);
	while (heap_size(heap)) {
		err = (struct error *)heap_pop(heap);
		save_error(file, err);
		free(err);
	}
	heap_destroy(heap);
}

static void save_err_queue(FILE *file, queue_t *q)
{
	struct error *err;
	size_t len;

	len = queue_size(q);
	fwrite(&len, sizeof(size_t), 1, file);
	while (queue_size(q)) {
		err = (struct error *)queue_pop(q);
		save_error(file, err);
		free(err);
	}
}

static void save_page(FILE *file, struct page *page)
{
	fprintf(run_log, "saving '%s' with %zu/%zu errors\n",
		page->title, heap_size(&page->err_found),
		queue_size(&page->err_approved));

	save_string(file, page->title);
	save_string(file, page->timestamp);
	save_string(file, page->content);
	save_err_heap(file, &page->err_found);
	save_err_queue(file, &page->err_approved);

	free(page->title);
	free(page->content);
	free(page->forbidden);	/* No need to save this */
	free(page->timestamp);
}

/* Save a queue of pages, is destructive */
void save_pages(const char *filename, queue_t *q)
{
	FILE *file;
	struct page *page;
	size_t pagenum;

	file = fopen(filename, "w");
	if (file == NULL) {
		fprintf(stderr, "(!) Cannot save pages to %s\n", filename);
		return;
	}

	pagenum = queue_size(q);
	fwrite(&pagenum, sizeof(size_t), 1, file);
	while (queue_size(q)) {
		page = (struct page *)queue_pop(q);
		save_page(file, page);
		free(page);
	}

	fclose(file);
}


/** Loading **/

static size_t read_size(FILE *file)
{
	size_t tmp;
	fread(&tmp, sizeof(size_t), 1, file);
	return tmp;
}

static char *load_string(FILE *file)
{
	size_t len;
	char *str;

	len = read_size(file);
	str = malloc(len+1);
	fread(str, 1, len, file);
	str[len] = '\0';
	//printf("string [%d][%s]\n", len, str);
	return str;
}

static struct error *load_error(FILE *file)
{
	struct error *err = malloc(sizeof(struct error));
	err->index = read_size(file);
	err->size = read_size(file);
	err->replacement = load_string(file);
	//printf("error i=%d %d{%s}\n", err->index, err->size, err->replacement);
	return err;
}

static heap_t load_err_heap(FILE *file)
{
	//puts("heap");
	size_t size = read_size(file);
	heap_t heap;
	struct error *err;

	heap_init(&heap, size, cmp_error);
	while (size--) {
		err = load_error(file);
		heap_push(&heap, (void *)err);
	}

	return heap;
}

static queue_t load_err_queue(FILE *file)
{
	//puts("queue");
	size_t size = read_size(file);
	queue_t q;
	struct error *err;

	queue_init(&q);
	while (size--) {
		err = load_error(file);
		queue_push(&q, (void *)err);
	}

	return q;
}

static struct page *load_page(FILE *file)
{
	struct page *page = malloc(sizeof(struct page));
	page->title = load_string(file);
	//printf("title: %s\n", page->title);
	page->timestamp = load_string(file);
	page->content = load_string(file);
	page->forbidden = calloc(strlen(page->content), sizeof(int));
	find_forbidden(page);
	page->err_found = load_err_heap(file);
	page->err_approved = load_err_queue(file);

	fprintf(run_log, "loaded '%s' with %zu/%zu errors\n",
		page->title, heap_size(&page->err_found),
		queue_size(&page->err_approved));

	return page;
}

/* Load a queue of pages from file */
void load_pages(const char *filename, queue_t *q)
{
	FILE *file;
	size_t pagenum;
	struct page *page;

	/* No saved state exists */
	if (access(filename, F_OK) == -1) {
		fprintf(run_log, "load_pages: file '%s' not found\n", filename);
		return;
	}

	file = fopen(filename, "r");
	if (file == NULL) {
		fprintf(stderr, "(!) Cannot load pages from %s\n", filename);
		return;
	}

	fread(&pagenum, sizeof(size_t), 1, file);
	while (pagenum--) {
		page = load_page(file);
		queue_push(q, (void *)page);
	}

	fclose(file);
}
