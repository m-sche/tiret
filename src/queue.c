/*
 * queue.c
 * Implements queue and stack as a doubly-linked list.
 *
 * 2014--2016 Martin Scheubrein
 */

#include <stdlib.h>
#include "queue.h"

struct queue_item {
	void *value;
	struct queue_item *next;
	struct queue_item *prev;
};

#if 0
#include <stdio.h>
void __attribute__((unused)) print_queue(queue_t *q)
{
	printf("[%u occupied]\n", q->size);
	struct queue_item *qi = q->first;
	while (qi) {
		printf("  %s\n", (char *)qi->value);
		qi = qi->next;
	}
	printf("[reverse]\n");
	qi = q->last;
	while (qi) {
		printf("  %s\n", (char *)qi->value);
		qi = qi->prev;
	}
	putchar('\n');
}
#endif

void queue_init(queue_t *q)
{
	q->size = 0;
	q->first = NULL;
	q->last = NULL;
}

void queue_push(queue_t *q, void *value)
{
	struct queue_item *item;

	item = calloc(1, sizeof(struct queue_item));
	item->value = value;
	q->size++;

	if (q->first) {
		item->next = q->first;
		q->first->prev = item;
	} else {
		q->last = item;
	}
	q->first = item;
	//print_queue(q);
}

void queue_unpop(queue_t *q, void *value)
{
	struct queue_item *item;

	item = calloc(1, sizeof(struct queue_item));
	item->value = value;
	q->size++;

	if (q->last) {
		item->prev = q->last;
		q->last->next = item;
	} else {
		q->first = item;
	}
	q->last = item;
}

size_t queue_size(queue_t *q)
{
	return q->size;
}

void *queue_pop(queue_t *q)
{
	struct queue_item *victim = q->last;
	void *value = victim->value;

	q->last = victim->prev;
	if (q->last)
		q->last->next = NULL;
	else
		q->first = NULL;

	q->size--;
	free(victim);

	return value;
}

void *queue_unpush(queue_t *q)
{
	struct queue_item *victim = q->first;
	void *value = victim->value;

	q->first = victim->next;
	if (q->first)
		q->first->prev = NULL;
	else
		q->last = NULL;

	q->size--;
	free(victim);

	return value;
}
