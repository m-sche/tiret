/*
 * heap.h
 * Implements priority queue as a dynamically growing binary heap.
 *
 * 2014--2016 Martin Scheubrein
 */

#ifndef HEAP_H
#define HEAP_H

typedef struct heap {
	size_t size;
	size_t num;
	int (*compare)();
	void **item;
} heap_t;

void heap_init(heap_t *heap, size_t size, int (*compare)());
size_t heap_size(heap_t *heap);
void heap_push(heap_t *heap, void *value);
void *heap_value(heap_t *heap);
void *heap_pop(heap_t *heap);
void heap_destroy(heap_t *heap);

#endif
