/*
 * string.c
 * Dynamically growing C strings, replacements, utf-8 support.
 *
 * 2014--2016 Martin Scheubrein
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void str_append(char **destination, const char *source)
{
	if (source == NULL || *source == '\0')
		return;
	if (*destination == NULL) {
		*destination = strdup(source);
		return;
	}

	char *new = malloc(strlen(*destination)+strlen(source)+1);
	strcpy(new, *destination);
	strcat(new, source);
	free(*destination);
	*destination = new;
}

void str_n_append(char **destination, const char *source, size_t n)
{
	if (source == NULL || n == 0)
		return;
	if (*destination == NULL) {
		*destination = strndup(source, n);
		return;
	}

	char *new = malloc(1+strlen(*destination)+
		(n > strlen(source) ? strlen(source) : n));
	strcpy(new, *destination);
	strncat(new, source, n);
	free(*destination);
	*destination = new;
}

/* Return 'haystack', where all occurences of 'needle' are replaced by 'replacement' */
char *replace(char *haystack, char *needle, char *replacement)
{
	size_t len = strlen(needle);
	char *sub = strstr(haystack, needle);
	char *str = NULL;

	while (sub) {
		str_n_append(&str, haystack, sub-haystack);
		str_append(&str, replacement);
		haystack = sub+len;
		sub = strstr(haystack, needle);
	}
	str_append(&str, haystack);
	return str;
}

/* Characters in utf8 string */ /* TODO Combining characters */
size_t mb_strnlen(const char *str, size_t n)
{
	size_t len;

	for (len = 0; *str && n--; str++) {
		if (!(*str & 0x80) || (*str & 0x40))
			len++;
	}
	return len;
}
