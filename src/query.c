/*
 * query.c
 * Download an article an possibly a list of random titles
 * through the MediaWiki API.
 *
 * 2014--2016 Martin Scheubrein
 */

#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include "common.h"
#include "heap.h"
#include "queue.h"

/* Return allocated string with 1-byte representation of the escape sequences */
static void unescape(char **str)
{
	const char *from = "abfnrtv\'\"\\";
	const char *to = "\a\b\f\n\r\t\v\'\"\\";
	size_t escape;

	char *out = NULL;
	char *index = *str;
	char *backslash = strchr(*str, '\\');

	while (backslash) {
		str_n_append(&out, index, backslash-index);
		escape = strchr(from, *(backslash+1))-from;
		str_n_append(&out, to+escape, 1);
		index = backslash+2;
		backslash = strchr(index, '\\');
	}
	str_append(&out, index);

	free(*str);
	*str = out;
}

#if defined(FAKE_QUERY) || defined(FAKE_RANDOM_LIST)
#include <stdio.h>
static __attribute__((unused)) char *read_file(const char *filename)
{
	FILE *file;
	size_t size;
	char *str;

	file = fopen(filename, "r");
	fseek(file, 0L, SEEK_END);
	size = ftell(file);
	rewind(file);
	str = malloc(size+1);
	fgets(str, size+1, file);
	fclose(file);
	return str;
}
#endif

/* Download a given page */
struct page query(CURL *curl, const char *base_url, const char *title)
{
	char *url = strdup(base_url);
	char *title_urlencoded;
	char *str;
	struct page page = {NULL};

	title_urlencoded = curl_easy_escape(curl, title, 0);

	str_append(&url, "?action=query&prop=revisions"
			"&format=json&utf8"
			"&rvprop=content|timestamp"
			"&redirects&titles=");
	str_append(&url, title_urlencoded);
	curl_free(title_urlencoded);

#ifdef FAKE_QUERY
	str = read_file(FAKE_QUERY);
#else
	str = curl_request(curl, url, NULL, 0);
#endif

	free(url);
	if (str == NULL)
		return page;

	/* For debugging */
	//puts(str); exit(0);

	/* TODO Prettify */
	if (strchr(str, '\n')) {
		//fprintf(run_log, "(!) query: LF found in '%s'\n", title);
		char *tmp = str;
		str = replace(tmp, "\n", "");
		free(tmp);
	}
	if (strchr(str, '\r')){
		//fprintf(run_log, "(!) query: CR found in '%s'\n", title);
		char *tmp = str;
		str = replace(tmp, "\r", "");
		free(tmp);
	}

	page.title = strdup(title);
	page.content = find_value(str, "query", "pages", "", "revisions", "1", "*", NULL);
	page.timestamp = find_value(str, "query", "pages", "", "revisions", "1", "timestamp", NULL);
	free(str);
	if (page.content == NULL)
		return page;

	unescape(&page.content);

#ifdef CHECK_IO
	char *fn = NULL;
	str_append(&fn, "tmp/");
	str_append(&fn, page.title);
	FILE *f = fopen(fn, "w");
	fputs(page.content, f);
	fclose(f);
#endif

	/* Initialize the rest of 'struct page' */
	page.forbidden = calloc(strlen(page.content), sizeof(int));
	heap_init(&page.err_found, 16, cmp_error);
	queue_init(&page.err_approved);

	return page;
}

#define XSTRINGIFY(str) STRINGIFY(str)
#define STRINGIFY(str) #str
#define RANDOM_LIMIT_STR XSTRINGIFY(RANDOM_LIMIT)

static char *get_random_list(CURL *curl, const char *base_url)
{
	char *url = strdup(base_url);
	char *str;

	str_append(&url, "?action=query&list=random"
			 "&format=json&utf8&rnnamespace=0"
			 "&rnlimit=" RANDOM_LIMIT_STR);

#ifdef FAKE_RANDOM_LIST
	str = read_file(FAKE_RANDOM_LIST);
#else
	str = curl_request(curl, url, NULL, 0);
#endif

	free(url);
	if (str == NULL)
		return NULL;
	//printf("%s\n", str);

	return str;
}

static void chop_json_list(char *list, queue_t *queue)
{
	char number[5];
	char *title;
	size_t i;

	for (i = 0; i < RANDOM_LIMIT; i++) {
		snprintf(number, 5, "%d", i+1);
		title = find_value(list, "query", "random", number, "title", NULL);
		queue_push(queue, title);
	}
}

/* Return a page, perform whatever operations required to satisfy */
struct page get_page(CURL *curl, const char *base_url)
{
	static int initialized = 0;
	static queue_t dwl_queue;
	char *title;
	struct page page;

	if (!initialized) {
		queue_init(&dwl_queue);
		initialized = 1;
	}

	if (queue_size(&dwl_queue) <= RANDOM_THRESHOLD) {
		fprintf(run_log, "get_page: downloading list\n");
		char *list = get_random_list(curl, base_url);
		chop_json_list(list, &dwl_queue);
		free(list);
	}

	title = (char *)queue_pop(&dwl_queue);
	fprintf(run_log, "get_page: downloading '%s'\n", title);
	page = query(curl, base_url, title);
	free(title);
	return page;
}
