/*
 * curl.c
 * Wrapper for client-server communication through libcurl.
 *
 * 2014--2016 Martin Scheubrein
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <curl/curl.h>
#include <pthread.h>
#include "common.h"

pthread_mutex_t curl_mutex = PTHREAD_MUTEX_INITIALIZER;

static size_t curl_write(void *ptr, size_t size, size_t nmemb, void *userdata)
{
	str_append((char **)userdata, (char *)ptr);
	return size*nmemb;
}

static void curl_wait(int instant)
{
	static time_t last_request = 0;
	time_t this_request;

	time(&this_request);

	if (!instant && this_request-last_request < CURL_BLOCKING)
		sleep(CURL_BLOCKING+last_request-this_request);
	last_request = this_request;
}

char *curl_request(CURL *curl, const char *url, const char *post, int flags)
{
	char *result = NULL;
	CURLcode retval;

	pthread_mutex_lock(&curl_mutex);

	curl_easy_setopt(curl, CURLOPT_URL, url);
	if (post)
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post);
	else
		curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curl_write);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&result);

	//fprintf(stderr, "GET : %s\n", url);
	//fprintf(stderr, "POST: %s\n", post);

	curl_wait(flags & CURL_INSTANT);
	retval = curl_easy_perform(curl);
	if (retval != CURLE_OK) {
		fprintf(stderr, "(!) curl: %s\n",
			curl_easy_strerror(retval));
		exit(1);
	}

	pthread_mutex_unlock(&curl_mutex);

	return result;
}
