/*
 * common.h
 * Prototypes of the public functions, configuration and debugging options.
 *
 * 2014--2016 Martin Scheubrein
 */

#ifndef COMMON_H
#define COMMOH_H

#include <curl/curl.h>
#include "heap.h"
#include "queue.h"

/** Options **/

/* main.c */
#define LOGIN	"m-sche"

/* curl.c */
#define CURL_BLOCKING 15	/* Seconds between particular requests */

/* query.c */
#define RANDOM_THRESHOLD 1	/* When only # titles remain, download new */
#define RANDOM_LIMIT     10	/* Download # titles at once */

/* main.c */
#define INPUT_THRESHOLD 12	/* Have # pages in input queue */


/** Debugging options **/

#define RUN_REAL		/* Real run. No fakes. Everything can fail. */
//#define RUN_SANDBOX		/* Real editations, but only in a sandbox */


/* Create files of articles */
//#define CHECK_IO

/* No requests at all */
#define FAKE_QUERY "hvezda.wiki"

/* Request the same titles */
//#define FAKE_RANDOM_LIST "random_list.wiki"

/* Operate (query & edit) on this title only */
//#define FAKE_TITLE "Wikipedista:M-sche/Pískoviště"

/* Make no destructive actions, no value */
#define FAKE_EDIT

/* Use monolithic architecture */
#define MONOLITHIC

/* Disable logging to history.log */
#define FAKE_LOG

/* Disable saving/loading */
#define FAKE_SAVE

#ifdef FAKE_QUERY
  #define FAKE_EDIT
  #undef FAKE_TITLE
#endif

#ifdef FAKE_TITLE
  #undef INPUT_THRESHOLD
  #define INPUT_THRESHOLD 1
#endif


#ifdef RUN_REAL
  #undef FAKE_QUERY
  #undef FAKE_RANDOM_LIST
  #undef FAKE_EDIT
  #undef MONOLITHIC
  #undef FAKE_LOG
  #undef FAKE_SAVE
  #undef FAKE_TITLE
#endif

#ifdef RUN_SANDBOX
  #undef FAKE_TITLE
  #define FAKE_TITLE "Wikipedista:M-sche/Pískoviště"
  #undef FAKE_QUERY
  #undef FAKE_EDIT
  #define FAKE_LOG
  #define FAKE_SAVE
  #undef INPUT_THRESHOLD
  #define INPUT_THRESHOLD 1
#endif


/** Prototypes **/

/* main.c */

void log_history(const char *fmt, ...);

/* parse.c */

char *find_value(char *str, ...);

/* string.c */

void str_append(char **destination, const char *source);
void str_n_append(char **destination, const char *source, size_t n);
char *replace(char *haystack, char *needle, char *replacement);
size_t mb_strnlen(const char *str, size_t n);

/* login.c */

void login(CURL *curl, const char *base_url, const char *username, const char *password);

/* query.c */

struct page {
	char *title;
	char *timestamp;
	char *content;
	int *forbidden;
	heap_t err_found;
	queue_t err_approved;
};

struct page query(CURL *curl, const char *base_url, const char *title); /* TODO make static */
struct page get_page(CURL *curl, const char *base_url);

/* edit.c */

#define ERR_EDIT_UNKNOWN	1
#define ERR_EDIT_CONFLICT	2

char *get_edit_token(CURL *curl, const char *base_url);
int edit(CURL *curl, const char *base_url, struct page page,
		const char *section, const char *summary, const char *token);

/* typography.c */

struct error {
	size_t index;
	size_t size;
	char *replacement;
};

/* For heap initialization */
static inline int cmp_error(void *a, void *b)
{
	int i = ((struct error *)a)->index;
	int j = ((struct error *)b)->index;
	return (i > j) - (i < j);
}

void context(char *str, struct error *err, size_t *from, size_t *to, size_t context);
void find_forbidden(struct page *page);
void find_errors(struct page *page);
char *repair(char *str, queue_t *errors);

/* ui.c */

int ui_approve(struct page *page);

/* curl.c */

#define CURL_INSTANT 0x01

char *curl_request(CURL *curl, const char *url, const char *post, int flags);

/* save.c */

void save_pages(const char *filename, queue_t *q);
void load_pages(const char *filename, queue_t *q);


/** Globals **/

/* Logging (main.c) */
extern FILE *run_log;	/* Non-persistent, queue changes + network I/O */
extern FILE *history_log;	/* Persistent, titles + found errors */

#endif
