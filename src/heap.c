/*
 * heap.c
 * Implements priority queue as a dynamically growing binary heap.
 *
 * 2014--2016 Martin Scheubrein
 */

#include <stdlib.h>
#include "heap.h"

void heap_init(heap_t *heap, size_t size, int (*compare)())
{
	heap->size = size;
	heap->num = 0;
	heap->compare = compare;
	heap->item = malloc(size*sizeof(void *));
	/* if (heap->item == NULL) well, it would crash anyway, so.. */
}

size_t heap_size(heap_t *heap)
{
	return heap->num;
}

static void heap_swap(heap_t *heap, size_t a, size_t b)
{
	void *tmp = heap->item[a];
	heap->item[a] = heap->item[b];
	heap->item[b] = tmp;
}

static int heap_cmp(heap_t *heap, size_t a, size_t b)
{
	return heap->compare(heap->item[a], heap->item[b]);
}

static void heap_realloc(heap_t *heap, size_t size)
{
	heap->item = realloc(heap->item, size*sizeof(void *));
	heap->size = size;
}

void heap_push(heap_t *heap, void *value)
{
	size_t i = heap->num;

	if (i == heap->size)
		heap_realloc(heap, heap->size*2);

	heap->item[i] = value;
	while (i && heap_cmp(heap, i, (i-1)/2) < 0) {
		heap_swap(heap, i, (i-1)/2);
		i = (i-1)/2;
	}

	heap->num++;
}

void *heap_value(heap_t *heap)
{
	return heap->item[0];
}

void *heap_pop(heap_t *heap)
{
	void *value = heap->item[0];
	size_t i = 0;

	heap->num--;
	heap->item[0] = heap->item[heap->num];
	heap->item[heap->num] = NULL;
	while (1) {
		/* Left child is violating */
		if (2*i+1 < heap->num && heap_cmp(heap, 2*i+1, i) < 0) {
			/* Right child is violating more */
			if (2*i+2 < heap->num && heap_cmp(heap, 2*i+2, 2*i+1) < 0) {
				heap_swap(heap, i, 2*i+2);
				i = 2*i+2;
			} else {
				heap_swap(heap, i, 2*i+1);
				i = 2*i+1;
			}
		/* Right child is violating */
		} else if (2*i+2 < heap->num && heap_cmp(heap, 2*i+2, i) < 0) {
			heap_swap(heap, i, 2*i+2);
			i = 2*i+2;
		} else {
			break;
		}
	}

	return value;
}

void heap_destroy(heap_t *heap)
{
	free(heap->item);
	heap->item = NULL;
}
